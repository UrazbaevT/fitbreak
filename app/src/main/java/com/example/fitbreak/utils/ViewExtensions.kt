package com.example.fitbreak.utils

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

fun View.loadImage(url: String) {
    Glide.with(this).load(url).centerCrop().into(this as ImageView)
}

//fun Fragment.showToast(msg: String) {
//    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
//}

