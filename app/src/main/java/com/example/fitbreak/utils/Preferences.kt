package com.example.fitbreak.utils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

class Preferences(context: Context) {
    private val sharedPref: SharedPreferences =
        context.getSharedPreferences("preferences", MODE_PRIVATE)

    //image
    fun getImageUri(): String {
        return sharedPref.getString("img", "").toString()
    }

    fun saveImageUri(ImgUri: String) {
        return sharedPref.edit().putString("img", ImgUri).apply()
    }

    fun isUserSeen(): Boolean{
        return sharedPref.getBoolean("onBoard", false)
    }

    fun saveSeen(){
        sharedPref.edit().putBoolean("onBoard", true).apply()
    }
}