package com.example.fitbreak

import android.os.Bundle
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.fitbreak.databinding.ActivityMainBinding
import com.example.fitbreak.utils.Preferences
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var pref: Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        pref = Preferences(this)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        if (!pref.isUserSeen())
            navController.navigate(R.id.onBoardFragment)

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_warm_up, R.id.navigation_profile, R.id.onBoardFragment, R.id.onBoardFragmentPage
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        val bottomNavFragments = arrayListOf(R.id.navigation_warm_up, R.id.navigation_profile)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            navView.isVisible = bottomNavFragments.contains(destination.id)
//           if (destination.id == R.id.onBoardFragment || ){
//               supportActionBar?.hide()
//           }else{
//               supportActionBar?.show()
//           }
            when(destination.id){
                R.id.onBoardFragment -> supportActionBar?.hide()
                R.id.onBoardFragmentPage -> supportActionBar?.hide()
                R.id.navigation_profile -> supportActionBar?.hide()
                R.id.navigation_warm_up -> supportActionBar?.hide()
                else -> supportActionBar?.show()
            }
        }
    }
}