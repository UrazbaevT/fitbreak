package com.example.fitbreak.ui.onBoard

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.fitbreak.ui.onBoard.adapter.AdapterPager
import com.example.fitbreak.databinding.FragmentOnBoardBinding
import com.example.fitbreak.utils.Preferences
import org.koin.android.ext.android.bind

class OnBoardFragment : Fragment() {

    private lateinit var binding: FragmentOnBoardBinding
    private lateinit var adapterPager: AdapterPager
    private lateinit var pref: Preferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentOnBoardBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pref = Preferences(requireContext())
        adapterPager = AdapterPager(requireActivity(),this::onSkipClick)
        binding.vpOnBoard.adapter = adapterPager
        binding.dotsIndicator.attachTo(binding.vpOnBoard)
    }

    private fun onSkipClick(){
        binding.vpOnBoard.currentItem = 2
    }
}