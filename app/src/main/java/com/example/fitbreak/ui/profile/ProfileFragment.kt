package com.example.fitbreak.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.fitbreak.databinding.FragmentProfileBinding
import com.example.fitbreak.utils.Preferences
import com.example.fitbreak.utils.loadImage

class ProfileFragment : Fragment() {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private lateinit var preference: Preferences

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(LayoutInflater.from(context), container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        preference = Preferences(requireContext())
        binding.imgProfile.loadImage(preference.getImageUri())//здесь картинка загружается на пользовательский экран
        initListeners()
//        showGallery()
    }

    private val mGetContent: ActivityResultLauncher<String> =
        registerForActivityResult(
            ActivityResultContracts.GetContent()// вместо байнд использую расширение глайда и сохраняю картинку
        ) { uri ->
            binding.imgProfile.loadImage(uri.toString())
            Preferences(requireContext()).saveImageUri(uri.toString())
        }


    private fun initListeners() {
        binding.imgProfile.setOnClickListener {
            mGetContent.launch("image/*")
        }
        binding.arrowBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }

//    private fun showGallery() {
//        childFragmentManager.beginTransaction()
//            .replace(R.id.container, Fragment())
//            .commit();
//    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}