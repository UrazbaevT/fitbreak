package com.example.fitbreak.ui.onBoard

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.example.fitbreak.R
import com.example.fitbreak.data.model.OnBoard
import com.example.fitbreak.ui.onBoard.adapter.AdapterPager.Companion.ON_BOARD_PAGE
import com.example.fitbreak.databinding.FragmentOnBoardPageBinding
import com.example.fitbreak.utils.Preferences


class OnBoardFragmentPage( private var listenerSkip:() -> Unit) : Fragment() {

    private lateinit var binding: FragmentOnBoardPageBinding
    private lateinit var pref: Preferences
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentOnBoardPageBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pref = Preferences(requireContext())
        onBoard()
    }

    private fun onBoard() {
        arguments.let {
            val data = it?.getSerializable(ON_BOARD_PAGE) as OnBoard
            binding.ivImage.setImageResource(data.img)
            binding.btn.text= data.btnText
            binding.tvTitle.text = data.title
            binding.tvDescOne.text= data.descOne
            binding.tvDescTwo.text = data.descTwo
            binding.tvDescThree.text = data.descThree
            binding.conLl.isVisible = data.isLast
            binding.radioGroup.isVisible = data.isR
            binding.radioOne.text = data.descOne
            binding.radioTwo.text = data.descTwo
            binding.radioThee.text = data.descThree
            binding.radioFour.text = data.descFour
            binding.radioFive.text = data.descFive
            binding.radioFive.isVisible = data.radioBtn
            binding.tvStep.text = data.step
        }

        binding.tvSkip.setOnClickListener {
            listenerSkip.invoke()
        }
        if (binding.btn.text.equals("Продолжить")){
            binding.btn.setOnClickListener {
                pref.saveSeen()
                findNavController().navigateUp()
            }
        }else if (binding.btn.text.equals("Включить")){
            binding.btn.setOnClickListener {
            findNavController().navigate(R.id.navigation_profile)
        }}
    }
}