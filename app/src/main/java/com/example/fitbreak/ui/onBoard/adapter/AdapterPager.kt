@file:Suppress("DEPRECATION")

package com.example.fitbreak.ui.onBoard.adapter

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.fitbreak.R
import com.example.fitbreak.data.model.OnBoard
import com.example.fitbreak.ui.onBoard.OnBoardFragmentPage


class AdapterPager(
    fragment: FragmentActivity,
    private var listenerSkip: () -> Unit,
) : FragmentStateAdapter(fragment) {

    private val listBoarding = arrayOf(
        OnBoard(
            "Шаг 1/3", "Хотите настроить уведомления\nдля FitBreak?", R.drawable.messages,
            isLast = true,
            isR = false,
            descOne = "Ежедневное уведомление",
            descTwo = "Мотивационные напоминания",
            descThree = "Персонализация",
            descFour = "",
            descFive = "",
            btnText = "Включить",
             false, false
        ),
        OnBoard(
            "Шаг 2/3", "Какую цель Вы хотите достичь\nс помощью FitBreak?", R.drawable.target,
            isLast = false,
            isR = true,
            descOne = "Бодрость и продуктивность",
            descTwo = "Устранение усталости",
            descThree = "Улучшение фокусировки",
            descFour = "Разминка суставов и мышц",
            descFive = "",
            btnText = "Продолжить",
             false, true
        ),
        OnBoard(
            "Шаг 3/3", "Выберите ваш график", R.drawable.notifi,
            isLast = false,
            isR = true,
            descOne = "9:00-18:00 - обед 12:00-13:00",
            descTwo = "8:30-17:30 - обед 12:00-13:00 ",
            descThree = "8:00-17:00 - обед 12:00-13:00",
            descFour = "10:00-22:00 - бед обеда",
            descFive = "ненормированный / другой",
            btnText = "Продолжить",
             true, false
        )
    )

    override fun getItemCount(): Int = listBoarding.size

    override fun createFragment(position: Int): Fragment {
        val data = bundleOf(ON_BOARD_PAGE to listBoarding[position])
        val fragment = OnBoardFragmentPage(listenerSkip)
        fragment.arguments = data
        return fragment
    }

    companion object {
        const val ON_BOARD_PAGE = "onBoard"
    }
}