package com.example.fitbreak.ui.warmUp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.fitbreak.data.model.WarmUp
import com.example.fitbreak.databinding.ItemWarmUpBinding

class WarmUpAdapter(var list: ArrayList<WarmUp>, private val onClick: (WarmUp) -> Unit) :
    Adapter<WarmUpAdapter.WarmUpViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WarmUpViewHolder {
        return WarmUpViewHolder(
            ItemWarmUpBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: WarmUpViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class WarmUpViewHolder(private val binding: ItemWarmUpBinding) :
        ViewHolder(binding.root) {
        fun bind(warmUp: WarmUp) {
            warmUp.img?.let { binding.imgItem.setImageResource(it) }
            binding.tvTitle.text = warmUp.title

            itemView.setOnClickListener {
                onClick(warmUp)
            }
        }

    }
}