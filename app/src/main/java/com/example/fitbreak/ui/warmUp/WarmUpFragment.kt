package com.example.fitbreak.ui.warmUp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.fitbreak.R
import com.example.fitbreak.data.model.WarmUp
import com.example.fitbreak.databinding.FragmentWarmUpBinding
import com.example.fitbreak.ui.warmUp.adapter.WarmUpAdapter

class WarmUpFragment : Fragment() {

    private var _binding: FragmentWarmUpBinding? = null
    private lateinit var adapter: WarmUpAdapter
    private var list = arrayListOf<WarmUp>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWarmUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadList()
        adapter = WarmUpAdapter(list, this::onClick)
        binding.rvWarmUp.layoutManager = LinearLayoutManager(requireContext())
        binding.rvWarmUp.adapter = adapter

    }

    private fun loadList() {
        list.add(WarmUp(R.drawable.img_wu_eyes, "Разминка для глаз"))
        list.add(WarmUp(R.drawable.img_wu_neck, "Разминка для шеи"))
        list.add(WarmUp(R.drawable.img_wu_hands, "Разминка для рук"))
        list.add(WarmUp(R.drawable.img_wu_legs, "Разминка для ног"))
        list.add(WarmUp(R.drawable.img_wu_back, "Разминка для спины"))
    }

    private fun onClick(warmUp: WarmUp){

    }
}