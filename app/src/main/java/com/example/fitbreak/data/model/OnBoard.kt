package com.example.fitbreak.data.model

import java.io.Serializable

data class OnBoard (
   val step : String,
   val title : String,
   val img : Int,
   val isLast : Boolean,
   val isR: Boolean,
   val descOne : String,
   val descTwo: String,
   val descThree: String,
   val descFour: String,
   val descFive: String,
   val btnText: String,
   val radioBtn : Boolean,
   val tv : Boolean
   ): Serializable