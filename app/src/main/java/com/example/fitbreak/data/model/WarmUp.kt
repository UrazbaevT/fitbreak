package com.example.fitbreak.data.model

data class WarmUp(
    var img: Int? = null,
    var title: String? = null
)